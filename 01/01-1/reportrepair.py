#!/usr/bin/env python3

with open("./input", "r") as input_file:
    entries = input_file.readlines()

    for i in range(len(entries)):
        for j in range(i+1, len(entries)):
            if int(entries[i]) + int(entries[j]) == 2020:
                print(int(entries[i]) * int(entries[j]))
                break
