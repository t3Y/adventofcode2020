#!/usr/bin/env python3

with open("./input", "r") as input_file:
    entries = input_file.readlines()

    for i in range(len(entries)):
        for j in range(i+1, len(entries)):
            for k in range(j+1, len(entries)):
                entry_1 = int(entries[i])
                entry_2 = int(entries[j])
                entry_3 = int(entries[k])
                if entry_1 + entry_2 + entry_3 == 2020:
                    print(entry_1 * entry_2 * entry_3)
                    break
