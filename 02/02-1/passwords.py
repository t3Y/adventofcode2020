#!/usr/bin/env python3


def check_pwd(password, policy_char, min_occurances, max_occurances):
    """
        return 1 if valid, 0 if not
    """
    occurances = 0
    for char in password:
        if char == policy_char:
            occurances += 1

    if occurances >= min_occurances and occurances <= max_occurances:
        return 1
    return 0


def main():
    with open("input", "r") as input_file:
        entries = input_file.readlines()
        valid_entries = 0

        for entry in entries:
            chunks = entry.split(" ")
            min_occurances = int(chunks[0].split("-")[0])
            max_occurances = int(chunks[0].split("-")[1])
            character = chunks[1][0]
            password = chunks[2].rstrip()
            valid_entries += check_pwd(password, character, min_occurances, max_occurances)

        print(valid_entries)


if __name__ == "__main__":
    main()
