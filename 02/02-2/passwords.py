#!/usr/bin/env python3


def check_pwd(password, policy_char, first_pos, second_pos):
    """
        return 1 if valid, 0 if not
    """
    set_first_pos = password[first_pos-1] == policy_char
    set_second_pos = password[second_pos-1] == policy_char

    if (set_first_pos or set_second_pos) and not (set_first_pos and set_second_pos):
        return 1
    return 0


def main():
    with open("input", "r") as input_file:
        entries = input_file.readlines()
        valid_entries = 0

        for entry in entries:
            chunks = entry.split(" ")
            first_pos = int(chunks[0].split("-")[0])
            second_pos = int(chunks[0].split("-")[1])
            character = chunks[1][0]
            password = chunks[2].rstrip()
            valid_entries += check_pwd(password, character, first_pos, second_pos)

        print(valid_entries)


if __name__ == "__main__":
    main()
