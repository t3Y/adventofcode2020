#!/usr/bin/env python3


def is_tree(char):
    if char == "#":
        return 1
    return 0


def main():
    with open("./input", "r") as input_file:
        map_layout = [line.rstrip() for line in input_file.readlines()]

    right = 3
    down = 1

    x_pos = 0
    y_pos = 0

    encountered_trees = 0

    while y_pos < len(map_layout):
        encountered_trees += is_tree(map_layout[y_pos][x_pos])

        x_pos = (x_pos + right) % len(map_layout[y_pos])
        y_pos += down

    print(encountered_trees)


main()
