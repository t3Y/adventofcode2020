#!/usr/bin/env python3


def is_tree(char):
    if char == "#":
        return 1
    return 0


def check_params(right, down, map_layout):
    x_pos = 0
    y_pos = 0

    encountered_trees = 0

    while y_pos < len(map_layout):
        encountered_trees += is_tree(map_layout[y_pos][x_pos])

        x_pos = (x_pos + right) % len(map_layout[y_pos])
        y_pos += down

    return encountered_trees


def main():
    with open("./input", "r") as input_file:
        map_layout = [line.rstrip() for line in input_file.readlines()]

    params = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
    aggr_res = 1
    for param in params:
        aggr_res *= check_params(param[0], param[1], map_layout)

    print(aggr_res)


main()
