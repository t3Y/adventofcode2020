#!/usr/bin/env python3


def check_passport(raw_passport):
    items = {}
    for entry in raw_passport.split(" "):
        parts = entry.split(":")
        key = parts[0]
        val = parts[1]
        items[key] = val

    if (len(items.keys()) == 8) or (len(items.keys()) == 7 and "cid" not in items.keys()):
        return 1
    return 0


def main():

    with open("input", "r") as input_file:
        data = input_file.readlines()

    raw_passport = ""
    valid_passports = 0
    for line in data:
        raw_passport += line.rstrip() + " "
        if line == "\n":
            valid_passports += check_passport(raw_passport.rstrip())
            raw_passport = ""

    print(valid_passports)


if __name__ == "__main__":
    main()
