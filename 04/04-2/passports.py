#!/usr/bin/env python3


def check_passport(raw_passport):
    items = {}
    for entry in raw_passport.split(" "):
        parts = entry.split(":")
        key = parts[0]
        val = parts[1]
        items[key] = val

    if not ((len(items.keys()) == 8) or (len(items.keys()) == 7 and "cid" not in items.keys())):
        return 0

    byr = int(items["byr"])
    if byr < 1920 or byr > 2002:
        return 0
    iyr = int(items["iyr"])
    if iyr < 2010 or iyr > 2020:
        return 0
    eyr = int(items["eyr"])
    if eyr < 2020 or eyr > 2030:
        return 0
    hgt = int(items["hgt"][:-2])
    hgt_u = items["hgt"][-2:]
    if hgt_u == "in":
        if hgt < 59 or hgt > 76:
            return 0
    elif hgt_u == "cm":
        if hgt < 150 or hgt > 193:
            return 0
    else:
        return 0
    hcl = items["hcl"]
    if len(hcl) != 7 or hcl[0] != "#":
        return 0
    for char in hcl[1:]:
        if char not in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]:
            return 0
    ecl = items["ecl"]
    if ecl not in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]:
        return 0
    pid = items["pid"]
    if len(pid) != 9:
        return 0
    return 1


def main():

    with open("input", "r") as input_file:
        data = input_file.readlines()

    raw_passport = ""
    valid_passports = 0
    for line in data:
        raw_passport += line.rstrip() + " "
        if line == "\n":
            valid_passports += check_passport(raw_passport.rstrip())
            raw_passport = ""

    print(valid_passports)


if __name__ == "__main__":
    main()
