#!/usr/bin/env python3
from math import ceil


def bsp(start_range, u_char, l_char, instruction):
    for char in instruction:
        if char == l_char:
            start_range[1] = start_range[1] - ceil((start_range[1] - start_range[0]) / 2)
        elif char == u_char:
            start_range[0] = start_range[0] + ceil((start_range[1] - start_range[0]) / 2)
    return start_range


def parse_ticket(ticket):
    seat_id = 0
    row_str = ticket[:7]
    col_str = ticket[7:]

    row = [0, 127]
    col = [0, 7]

    row = bsp(row, "B", "F", row_str)
    col = bsp(col, "R", "L", col_str)

    seat_id = row[1] * 8 + col[1]

    return seat_id


def main():

    with open("./input", "r") as input_file:
        tickets = [ticket.rstrip() for ticket in input_file.readlines()]

    ids = []
    for ticket in tickets:
        new_id = parse_ticket(ticket)
        ids.append(new_id)

    ids = sorted(ids)

    missing_id = None
    for i in range(1, len(ids)):
        if ids[i] - ids[i-1] == 2:
            missing_id = ids[i]-1
            break
    print(missing_id)


if __name__ == "__main__":
    main()
