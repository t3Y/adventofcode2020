#!/usr/bin/env python3


def count_positives(symbol):
    chars = [char for char in symbol]

    return len(set(chars))


def main():

    with open("input", "r") as input_file:

        input_lines = [line.rstrip() for line in input_file.readlines()]

    total = 0
    current_symbol = ""
    for line in input_lines:

        if len(line) > 0:
            current_symbol += line
        else:
            total += count_positives(current_symbol)
            current_symbol = ""

    print(total)


if __name__ == "__main__":
    main()
