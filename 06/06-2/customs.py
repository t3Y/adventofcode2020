#!/usr/bin/env python3


def count_positives(symbols):
    intersection = set(symbols[0])
    for symbol in symbols:
        intersection = intersection.intersection(symbol)

    return len(intersection)


def main():

    with open("input", "r") as input_file:

        input_lines = [line.rstrip() for line in input_file.readlines()]

    total = 0
    current_symbol = []
    for line in input_lines:

        if len(line) > 0:
            current_symbol.append(line)
        else:
            total += count_positives(current_symbol)
            current_symbol = []

    print(total)


if __name__ == "__main__":
    main()
