#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        lines = [line.rstrip() for line in input_file.readlines()]

    parents = {}
    childrens = {}

    for line in lines:

        bits = line.split(" bags contain")
        parent = bits[0]
        children = [word.rstrip(".") for word in bits[1].split(",")]
        count = [0] * len(children)
        for i in range(len(children)):
            count[i] = children[i][1]
            children[i] = children[i][3:].replace(" bags", "").replace(" bag", "")

            if children[i] not in parents.keys():
                parents[children[i]] = [parent]
            else:
                parents[children[i]].append(parent)
        childrens[parent] = children

    shiny_gold_ancestors = find_ancestors(set(["shiny gold"]), parents)
    print(len(shiny_gold_ancestors))


def find_ancestors(orig, parents):

    ancestors = set()
    unvisited_ancestors = orig
    while len(unvisited_ancestors) > 0:

        new_ancestors = set()

        for bag in unvisited_ancestors:

            if bag not in parents.keys():
                continue
            for parent in parents[bag]:
                if parent not in ancestors:
                    new_ancestors.add(parent)

        ancestors = ancestors.union(new_ancestors)
        unvisited_ancestors = new_ancestors

    return ancestors


if __name__ == "__main__":
    main()
