#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        lines = [line.rstrip() for line in input_file.readlines()]

    parents = {}
    childrens = {}

    for line in lines:

        bits = line.split(" bags contain")
        parent = bits[0]
        children = [word.rstrip(".") for word in bits[1].split(",")]
        if "no other" in children[0]:
            children = []
        count = [0] * len(children)
        for i in range(len(children)):
            count[i] = children[i][1]
            children[i] = children[i][3:].replace(" bags", "").replace(" bag", "")

            if children[i] not in parents.keys():
                parents[children[i]] = [parent]
            else:
                parents[children[i]].append(parent)
        childrens[parent] = (children, count)

    print(count_content("shiny gold", childrens))


def count_content(bag, children):

    total = 0

    for i in range(len(children[bag][0])):
        entry = children[bag]
        child = entry[0][i]
        count = int(entry[1][i])

        total += count + count * count_content(child, children)
    return total


if __name__ == "__main__":
    main()
