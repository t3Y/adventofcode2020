#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:

        code = [line.rstrip() for line in input_file.readlines()]

    acc = 0
    pointer = 0
    history = set()

    while pointer not in history:

        history.add(pointer)

        instr = code[pointer]
        op = instr.split(" ")[0]
        val = int(instr.split(" ")[1])

        if op == "nop":
            pointer += 1
            continue
        if op == "acc":
            acc += val
            pointer += 1
            continue
        if op == "jmp":
            pointer += val
            continue
        print("wtf happened")

    print(acc)


if __name__ == "__main__":
    main()
