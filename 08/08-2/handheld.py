#!/usr/bin/env python3


def execute_program(code):

    acc = 0
    pointer = 0
    history = set()

    while pointer not in history:

        if pointer == len(code):
            return acc

        history.add(pointer)

        instr = code[pointer]
        op = instr.split(" ")[0]
        val = int(instr.split(" ")[1])

        if op == "nop":
            pointer += 1
            continue
        if op == "acc":
            acc += val
            pointer += 1
            continue
        if op == "jmp":
            pointer += val
            continue
        print("wtf happened")

    return None


def main():

    with open("input", "r") as input_file:

        code = [line.rstrip() for line in input_file.readlines()]

    for i in range(len(code)):
        if "nop" in code[i]:
            code[i] = code[i].replace("nop", "jmp")
            res = execute_program(code)
            if res is not None:
                print(res)
                return
            code[i] = code[i].replace("jmp", "nop")

        if "jmp" in code[i]:
            code[i] = code[i].replace("jmp", "nop")
            res = execute_program(code)
            if res is not None:
                print(res)
                return
            code[i] = code[i].replace("nop", "jmp")


if __name__ == "__main__":
    main()
