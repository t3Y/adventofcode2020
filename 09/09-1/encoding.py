#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        lines = [int(line.rstrip()) for line in input_file.readlines()]

    queue = lines[:25]

    for i in range(25, len(lines)):

        line = lines[i]
        valid = check_nb(queue, line)
        if valid:
            queue.pop(0)
            queue.append(line)
        else:
            print(line)
            return


def check_nb(queue, val):

    for i in range(len(queue)):
        for j in range(i, len(queue)):
            if queue[i] + queue[j] == val:
                return True
    return False


if __name__ == "__main__":
    main()
