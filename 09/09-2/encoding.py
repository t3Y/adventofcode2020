#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        lines = [int(line.rstrip()) for line in input_file.readlines()]

    target = get_invalid_entry(lines)

    for i in range(len(lines)):
        j = i+1
        seq = lines[i] + lines[j]
        while seq <= target:
            if seq == target:
                print(min(lines[i:j+1]) + max(lines[i:j+1]))
                return
            j += 1
            seq = sum_thru(i, j, lines)


def sum_thru(start, end, lines):

    total = 0
    for i in range(start, end+1):
        total += lines[i]
    return total


def get_invalid_entry(lines):
    queue = lines[:25]

    for i in range(25, len(lines)):

        line = lines[i]
        valid = check_nb(queue, line)
        if valid:
            queue.pop(0)
            queue.append(line)
        else:
            return line
    return None


def check_nb(queue, val):

    for i in range(len(queue)):
        for j in range(i, len(queue)):
            if queue[i] + queue[j] == val:
                return True
    return False


if __name__ == "__main__":
    main()
