#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        ratings = [int(line) for line in input_file.readlines()]

    ratings = sorted(ratings)
    ratings = [0] + ratings
    ratings.append(ratings[-1]+3)

    threes = 0
    ones = 0
    for i in range(1, len(ratings)):
        diff = ratings[i] - ratings[i-1]
        if diff == 3:
            threes += 1
        elif diff == 1:
            ones += 1
    print((ones * threes))


if __name__ == "__main__":
    main()
