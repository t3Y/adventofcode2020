#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        ratings = [int(line) for line in input_file.readlines()]

    ratings = sorted(ratings)
    ratings = [0] + ratings
    ratings.append(ratings[-1]+3)

    pos = 0
    cache = {}
    valid = check(pos, ratings, cache)
    print(valid)


def check(pos, ratings, cache):

    current_val = ratings[pos]
    if pos in cache.keys():
        return cache[pos]
    if pos >= len(ratings)-3:
        return 1

    after_val = ratings[pos+2]
    after_after_val = ratings[pos+3]

    splits = 0
    splits += check(pos+1, ratings, cache)
    if after_val - current_val > 3:
        cache[pos] = splits
        return splits
    splits += check(pos+2, ratings, cache)
    if after_after_val - current_val > 3:
        cache[pos] = splits
        return splits
    splits += check(pos+3, ratings, cache)
    cache[pos] = splits
    return splits


if __name__ == "__main__":
    main()
