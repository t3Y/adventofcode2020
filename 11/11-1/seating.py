#!/usr/bin/env python3
import copy


def main():

    with open("input", "r") as input_file:
        board = [list(line.rstrip()) for line in input_file.readlines()]

    change = True
    while change:
        board, change = step(board)

    occupied_seats = 0
    for line in board:
        for char in line:
            if char == "#":
                occupied_seats += 1
    print(occupied_seats)


def step(board):

    change = False

    new_state = copy.deepcopy(board)
    for i in range(len(board)):
        line = board[i]
        for j in range(len(line)):
            if line[j] == ".":
                continue

            active_neighbors = 0
            # top
            if i > 0:
                if j > 0:
                    # top left
                    if board[i-1][j-1] == "#":
                        active_neighbors += 1
                # top center
                if board[i-1][j] == "#":
                    active_neighbors += 1
                if j < len(line) - 1:
                    # top right
                    if board[i-1][j+1] == "#":
                        active_neighbors += 1
            # left
            if j > 0:
                if board[i][j-1] == "#":
                    active_neighbors += 1
            # right
            if j < len(line) - 1:
                if board[i][j+1] == "#":
                    active_neighbors += 1

            # bottom
            if i < len(board) - 1:
                if j > 0:
                    # bottom left:
                    if board[i+1][j-1] == "#":
                        active_neighbors += 1
                if board[i+1][j] == "#":
                    # bottom middle
                    active_neighbors += 1
                if j < len(line) - 1:
                    # bottom right
                    if board[i+1][j+1] == "#":
                        active_neighbors += 1

            if line[j] == "L" and active_neighbors == 0:
                new_state[i][j] = "#"
                change = True
            elif line[j] == "#" and active_neighbors >= 4:
                new_state[i][j] = "L"
                change = True

    return (new_state, change)


if __name__ == "__main__":
    main()
