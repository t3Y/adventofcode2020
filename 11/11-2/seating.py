#!/usr/bin/env python3
import copy


def main():

    with open("input", "r") as input_file:
        board = [list(line.rstrip()) for line in input_file.readlines()]

    change = True
    while change:
        board, change = step(board)

    occupied_seats = 0
    for line in board:
        for char in line:
            if char == "#":
                occupied_seats += 1
    print(occupied_seats)


def first_visible_seat(board, x, y, vector):

    upper_x = len(board)-1
    upper_y = len(board[0])-1

    while True:
        x += vector[0]
        y += vector[1]
        # are we still inbound?
        if x < 0 or y < 0 or x > upper_x or y > upper_y:
            return 0  # no seat visible
        if board[x][y] == "#":
            return 1  # occupied seat
        if board[x][y] == "L":
            return 0  # empty seat
        # floor, continue


def step(board):

    change = False

    new_state = copy.deepcopy(board)
    for i in range(len(board)):
        line = board[i]
        for j in range(len(line)):
            if line[j] == ".":
                continue

            active_neighbors = 0
            for mod_j in range(-1, 2):
                for mod_i in range(-1, 2):
                    if mod_i == 0 and mod_j == 0:
                        continue
                    active_neighbors += first_visible_seat(board, i, j, (mod_i, mod_j))

            if line[j] == "L" and active_neighbors == 0:
                new_state[i][j] = "#"
                change = True
            elif line[j] == "#" and active_neighbors >= 5:
                new_state[i][j] = "L"
                change = True

    return (new_state, change)


if __name__ == "__main__":
    main()
