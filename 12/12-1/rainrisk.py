#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        instructions = [line.rstrip() for line in input_file.readlines()]

    position = [0, 0]
    directions = [(1, 0), (0, -1), (-1, 0), (0, 1)]
    facing = 0

    for instruction in instructions:
        code = instruction[0]
        val = int(instruction[1:])

        if code == "N":
            position[1] += val
        elif code == "S":
            position[1] -= val
        elif code == "E":
            position[0] += val
        elif code == "W":
            position[0] -= val
        elif code == "R":
            facing = (facing + val // 90) % len(directions)
        elif code == "L":
            facing = (facing - val // 90) % len(directions)
        elif code == "F":
            position[0] += directions[facing][0] * val
            position[1] += directions[facing][1] * val

    print(abs(position[0] + abs(position[1])))


if __name__ == "__main__":
    main()
