#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        instructions = [line.rstrip() for line in input_file.readlines()]

    position = [0, 0]
    waypoint = [10, 1]

    for instruction in instructions:
        code = instruction[0]
        val = int(instruction[1:])

        if code == "N":
            waypoint[1] += val
        elif code == "S":
            waypoint[1] -= val
        elif code == "E":
            waypoint[0] += val
        elif code == "W":
            waypoint[0] -= val
        elif code == "R":
            waypoint = rotate(waypoint, "R", val // 90)
        elif code == "L":
            waypoint = rotate(waypoint, "L", val // 90)
        elif code == "F":
            position[0] += waypoint[0] * val
            position[1] += waypoint[1] * val

    print(abs(position[0] + abs(position[1])))


def rotate(vector, direction, times):
    order = (1, -1)
    if direction == "R":
        order = (-1, 1)

    while times > 0:
        new = [0, 0]
        new[0] = order[1] * vector[1]
        new[1] = order[0] * vector[0]
        vector = new
        times -= 1

    return vector


if __name__ == "__main__":
    main()
