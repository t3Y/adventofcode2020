#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        instructions = [line.rstrip() for line in input_file.readlines()]

    start = int(instructions[0])
    shuttles = instructions[1].split(",")
    active = []
    nearest = None
    nearest_id = None
    for shuttle in shuttles:
        if shuttle != "x":
            active.append(int(shuttle))

    for shuttle in active:
        mult = (start // shuttle) + 1
        candidate = mult * shuttle
        if nearest is None or candidate < nearest:
            nearest = candidate
            nearest_id = shuttle

    print((nearest - start) * nearest_id)


if __name__ == "__main__":
    main()
