#!/usr/bin/env python3
from functools import reduce


def main():

    with open("input", "r") as input_file:
        instructions = [line.rstrip() for line in input_file.readlines()]

    shuttles = instructions[1].split(",")
    constraints = {}

    for i in range(len(shuttles)):
        if shuttles[i] == "x":
            continue
        offset = i
        shuttle_id = int(shuttles[i])
        constraints[shuttle_id] = offset

    keys = sorted(constraints.keys(), reverse=True)
    keys = list(constraints.keys())

    valids = 0
    option = 0
    increment = keys[0]

    while valids < len(keys):
        option += increment
        curr_valids = count_valids(keys, option - constraints[keys[0]], constraints)
        if curr_valids > valids:
            valids = curr_valids
            # pattern repeats with a step of LCM(previous ids)
            increment = reduce(lambda x, y: x * y, keys[:valids], 1)

    print(option - constraints[keys[0]])


def count_valids(keys, candidate, constraints):
    valids = 0
    for key in keys:
        shuttle = key
        offset = constraints[key]
        if (candidate + offset) % shuttle == 0:
            valids += 1
        else:
            return valids
    return valids


if __name__ == "__main__":
    main()
