#!/usr/bin/env python3
import re


def main():

    with open("input", "r") as input_file:

        instructions = [line.rstrip() for line in input_file.readlines()]

    bitmask = ""
    memory = {}

    addr = re.compile("[0-9]+")

    for line in instructions:
        cmd = line.split(" = ")

        if "mask" in cmd[0]:
            bitmask = cmd[1]
        else:
            address = addr.search(cmd[0]).group()
            val = list(bin(int(cmd[1]))[2:].zfill(len(bitmask)))
            for i in range(len(bitmask)):
                char = bitmask[i]
                if char != "X":
                    val[i] = char
            memory[address] = val

    total = 0
    for value in memory.values():
        total += int("".join(value), 2)

    print(total)


if __name__ == "__main__":
    main()
