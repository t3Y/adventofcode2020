#!/usr/bin/env python3
import re


def main():

    with open("input", "r") as input_file:

        instructions = [line.rstrip() for line in input_file.readlines()]

    bitmask = ""
    memory = {}

    addr = re.compile("[0-9]+")

    ij = 0
    for line in instructions:
        ij += 1
        cmd = line.split(" = ")

        if "mask" in cmd[0]:
            bitmask = cmd[1]
        else:
            address = list(bin(int(addr.search(cmd[0]).group()))[2:].zfill(len(bitmask)))
            val = int(cmd[1])
            floats = []
            for i in range(len(bitmask)):
                char = bitmask[i]
                if char != "0":
                    address[i] = char
                if char == "X":
                    floats.append(i)
            addrs = get_all_addrs(address, floats)
            for real_addr in addrs:
                memory["".join(real_addr)] = val

    total = 0
    for value in memory.values():
        total += value

    print(total)


def get_all_addrs(address, given_floats):

    floats = given_floats[:]
    fork = floats.pop(0)

    path_0 = address[:]
    path_0[fork] = "0"
    path_1 = address[:]
    path_1[fork] = "1"

    if len(floats) == 0:
        return [path_0, path_1]
    else:
        return get_all_addrs(path_0, floats) + get_all_addrs(path_1, floats)


if __name__ == "__main__":
    main()
