#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        starts = [int(number) for number in input_file.read().rstrip().split(",")]

    """
    tests = {
            1: [1, 3, 2],
            10: [2, 1, 3],
            27: [1, 2, 3]
            }

    for res, test in tests.items():
        print("TEST: {} = {}, {}".format(test, res, play(test, 2020) == res))
    """

    print(play(starts, 2020))


def play(starts, until):
    memory = {}
    last = None
    turn = 1

    while turn <= until:
        if turn <= len(starts):
            number = starts[turn-1]
        else:
            if len(memory[last]) == 1:
                number = 0
            else:
                number = memory[last][-1] - memory[last][-2]

        last = number
        if number not in memory.keys():
            memory[number] = [turn]
        else:
            memory[number].append(turn)

        turn += 1
    return last


if __name__ == "__main__":
    main()
