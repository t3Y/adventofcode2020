#!/usr/bin/env python3


def main():

    with open("input", "r") as input_file:
        starts = [int(number) for number in input_file.read().rstrip().split(",")]

    """
    tests = {
            2578: [1, 3, 2],
            3544142: [2, 1, 3],
            261214: [1, 2, 3],
            175594: [0, 3, 6]
            }

    for res, test in tests.items():
        print("TEST: {} = {}, {}".format(test, res, play(test, 30000000) == res))
    """

    # dumb bruteforce works
    print(play(starts, 30000000))


def play(starts, until):
    memory = {}
    last = None
    turn = 1

    while turn <= until:
        if turn % 100000 == 0:
            print("{}/{}".format(turn, until))
        if turn <= len(starts):
            number = starts[turn-1]
        else:
            if len(memory[last]) == 1:
                number = 0
            else:
                number = memory[last][-1] - memory[last][-2]

        last = number
        if number not in memory.keys():
            memory[number] = [turn]
        else:
            memory[number].append(turn)

        turn += 1
    return last


if __name__ == "__main__":
    main()
